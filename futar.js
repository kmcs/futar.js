var BASEURL = "http://futar.bkk.hu/bkk-utvonaltervezo-api/ws/otp/api/where/",
    reloadBtn, mainNode, backBtnCont,
    jsonpCallbacks = {cntr: 0};

window.onload = function() {
    reloadBtn = document.getElementById("reload_btn");
    mainNode = document.getElementById("main");
    backBtnCont = document.getElementById("back");
    reloadBtn.onclick = getDepartures;
    document.getElementById("back_btn").onclick = function() {
        backBtnCont.style.display = "none";
        getDepartures();
    }
    getDepartures();
}

function getDepartures() {
    mainNode.innerHTML = "";
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
            getDataForId(BASEURL + "stops-for-location.json?lat=" + position.coords.latitude +
                "&lon=" + position.coords.longitude + "&radius=500&includeReferences=false",
                {parent: mainNode, coords: position.coords}, stops);
        });
    } else {
        var pNode = document.createElement("p");
        pNode.textContent = "Nem elérhető a helyzeted. :(";
        mainNode.appendChild(pNode);
    }
}

function getTripDetails(tripId) {
    mainNode.innerHTML = "";
    getDataForId(BASEURL + "trip-details.json?tripId=" + tripId + "&includeReferences=true",
        {parent: mainNode}, tripDetails);
}

function stops(params, stops) {
    var ul = document.createElement("ul"),
        stopList = [];
    for (var i = 0; i < stops.data.list.length; i++) {
        stopList.push({
            id: stops.data.list[i].id,
            name: stops.data.list[i].name,
            dist: calcDist(params.coords.latitude, params.coords.longitude,
                stops.data.list[i].lat, stops.data.list[i].lon),
        });
    }
    stopList.sort(function(a,b) {return a.dist - b.dist});

    for (var i = 0; i < stopList.length; i++) {
        var li = document.createElement("li"),
            stopSpan = document.createElement("span");
        stopSpan.classList.add("lh");
        stopSpan.textContent += stopList[i].name + " (" + stopList[i].dist + " m)";
        getDataForId(BASEURL + "arrivals-and-departures-for-stop.json?stopId=" +
            stopList[i].id, {parent: li}, departures);
        li.appendChild(stopSpan);
        ul.appendChild(li);
    }

    params.parent.appendChild(ul);
}

function departures(params, departures) {
    var ul = document.createElement("ul");
    for (var i = 0; i < departures.data.entry.stopTimes.length; i++) {
        var li = document.createElement("li"),
            routeNameSpan = document.createElement("span"),
            tripId = departures.data.entry.stopTimes[i].tripId,
            routeId = departures.data.references.trips[tripId].routeId,
            routeShortName = departures.data.references.routes[routeId].shortName,
            background = "#" + departures.data.references.routes[routeId].color,
            color = "#" + departures.data.references.routes[routeId].textColor,
            tripHeadsign = departures.data.references.trips[tripId].tripHeadsign,
            time = timeFmt(departures.data.entry.stopTimes[i].departureTime),
            predictedTime;
        if (departures.data.entry.stopTimes[i].predictedDepartureTime) {
            predictedTime = timeFmt(departures.data.entry.stopTimes[i].predictedDepartureTime);
        } else {
            predictedTime = undefined;
        }
        li.classList.add("le");
        routeNameSpan.classList.add("route_name");
        routeNameSpan.style.background = background;
        routeNameSpan.style.color = color;
        routeNameSpan.textContent = routeShortName;
        li.textContent = tripHeadsign + " ";
        if (predictedTime) {
            var predTimeSpan = document.createElement("span");
            predTimeSpan.classList.add("predicted_time");
            predTimeSpan.textContent = predictedTime;
            li.appendChild(predTimeSpan);
        } else {
            li.textContent += time;
        }
        li.insertBefore(routeNameSpan, li.firstChild);
        li.onclick = function() {
            backBtnCont.style.display = "block";
            reloadBtn.onclick = function() {
                getTripDetails(tripId);
            }
            getTripDetails(tripId);
        }
        ul.appendChild(li);
    }
    params.parent.appendChild(ul);
}

function tripDetails(params, tripDetails) {
    var ul = document.createElement("ul");
    for (var i = 0; i < tripDetails.data.entry.stopTimes.length; i++) {
        var li = document.createElement("li"),
            stopId = tripDetails.data.entry.stopTimes[i].stopId;
        li.classList.add("le");
        li.textContent = tripDetails.data.references.stops[stopId].name + " " +
            timeFmt(tripDetails.data.entry.stopTimes[i].departureTime);
        ul.appendChild(li);
    }
    mainNode.appendChild(ul);
}

function timeFmt(timestamp) {
    var date = new Date(timestamp * 1000),
        mins = date.getMinutes();
    return date.getHours() + ":" + (mins < 10 ? "0" + mins : mins);
}

function calcDist(lat0, lon0, lat1, lon1) {
    var dLat = rad(lat1 - lat0),
        dLon = rad(lon1 - lon0),
        a = Math.pow(Math.sin(dLat / 2), 2) +
            Math.cos(rad(lat0)) * Math.cos(rad(lat1)) *
            Math.pow(Math.sin(dLon / 2), 2),
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return Math.round(c * 6371000);
}

function rad(deg) {
    return deg * Math.PI / 180;
}

function getDataForId(url, params, callback) {
    var req = new XMLHttpRequest();
    req.addEventListener("load", function() {
        callback(params, JSON.parse(req.response));
    });
    req.open("GET", url);
    req.send();
}
